# Exercise 8-9: Messages
def show_messages(messages):
    '''Print each message inside a list'''
    for message in messages:
        print(message.title())

message_list = ['hello there', 'some random text', 'more random text', 'i think i\'m done']

## show_messages(message_list)


# Exercise 8-10: Sending Messages
# Build on top of exercise 8-9
def send_messages(messages, sent):
    '''Print and transfer messages to new list'''
    while messages:
        current_message = messages.pop()
        print(current_message)
        sent.append(current_message)

def transferred_messages(messages, sent):
    '''Print the original and new list'''
    print("Here's the original list: ")
    print(messages)
    print("\nHere's the new list: ")
    print(sent)

sent_messages = []

## send_messages(message_list, sent_messages)
## transferred_messages(message_list, sent_messages)

# Exercise 8-11: Archived Messages
# Call send_messages function with a COPY of the original list
send_messages(message_list[:], sent_messages)
transferred_messages(message_list, sent_messages)

