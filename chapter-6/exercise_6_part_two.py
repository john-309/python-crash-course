# Exercise 6-5
rivers = {
    'nile' : 'egypt',
    'sepik' : 'new guinea',
    'mississippi' : 'united states'
}

for river, country in rivers.items():
    print(f"The {river.title()} river runs through {country.title()}.")
print()


for river in rivers:
    print(river.title())
print()


for country in rivers.values():
    print(country.title())
print()


# Exercise 6-6
favorite_languages = {
    'jen': 'python',
    'sarah': 'c',
    'edward': 'ruby',
    'phil': 'python',
    }

people_poll_check = ['sam', 'sarah', 'elliot', 'phil', 'eric']

for name in people_poll_check:
    if name in favorite_languages:
        print(f"Thank you {name.title()} for taking the poll.")
    else:
        print(f"{name.title()}, please take the poll.")
