# Exercise 9-1: Create a Restaurant Class
class Restaurant:
    """A Simple Restaurant Class"""
    def __init__(self, name, cuisine_type):
        self.name = name
        self.cuisine = cuisine_type
    
    def describe_restaurant(self):
        print(f"The name of the restaurant is {self.name}")
        print(f"This restaurant sells {self.cuisine}")

    def open_restaurant(self):
        print(f"{self.name} is now open!")


first_restaurant = Restaurant('Chow House', 'Chicken')

print(first_restaurant.name)
print(first_restaurant.cuisine)

first_restaurant.describe_restaurant()
first_restaurant.open_restaurant()
print()


## Exercise 9-2: Instantiate 3 different restaraunts
second_restaurant = Restaurant('Hello', 'Fries')
third_restaurant = Restaurant('The Spot', 'Chicken Wings')
fourth_restaurant = Restaurant('Moo', 'Beef')

second_restaurant.describe_restaurant()
third_restaurant.describe_restaurant()
fourth_restaurant.describe_restaurant()
