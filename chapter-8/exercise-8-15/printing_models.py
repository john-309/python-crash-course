# import all functions from module
from printing_functions import *

# Start with some designs that need to be printed.
unprinted_designs = ['phone case', 'robot pendant', 'dodecahedron']
completed_models = []

# Pass lists as parameters to imported functions

print_design(unprinted_designs, completed_models)
complete_design(completed_models)

