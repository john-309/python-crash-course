# Use the range() function in for loop
for value in range(1, 5):
    print(value)
print()


# Use range() with one value
for value in range(6):
    print(value)
print()

# Save a range as a list using the list() function
numbers = list(range(1, 10))
print(numbers)

# Print a list with a step
odd_numbers = list(range(1, 11, 2))
print(odd_numbers)
