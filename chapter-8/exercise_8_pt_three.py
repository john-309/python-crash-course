# Exercise 8-6: City Names
def city_country(city, country):
    formatted_city = f"{city}, {country}"

    return formatted_city.title()

first_city_country = city_country('santiago', 'chile')
secong_city_country = city_country('buenos aires', 'argentina')

print(first_city_country)
print(secong_city_country)


# Exercise 8-7: Album
def make_album(artist, title, songs=None):
    artist_album = {'name': artist, 'album': title}

    if songs:
        artist_album['number of songs'] = songs
    
    return artist_album

# first_artist = make_album(artist='drake', title='something')
# second_artist = make_album('marvin gaye', 'here, my dear', 20)
#
# print(first_artist)
# print(second_artist)


# Exercise 8-8: User Albums
# Convert Exercise 8-7 into while loop with user input

print("Enter the name of an artist and album: ")
print("(press 'q' to quit) ")

while True:
    artist_name = input("Artist Name: ")
    if artist_name == 'q':
        break
    album_title = input("Album Title: ")
    if album_title == 'q':
        break
    number_songs = input("Number of songs (Press Enter/Return key if unkown): ")
    if number_songs == 'q':
        break

    formatted_artist_album = make_album(artist_name, album_title, number_songs)
    print(f"This is your favorite artist's album: {formatted_artist_album}")
    