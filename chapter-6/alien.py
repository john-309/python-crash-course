# Modifying values in a Dictionary
alien = {'x_position' : 0, 'y_position' : 25, 'speed' : 'medium', 'points' : 5 }

print(f"Original Position: {alien['x_position']}")


# Move the alien to the right
# Determine how far to move the alien based on its current speed.
if alien['speed'] == 'slow':
    x_increment =  1
elif alien['speed'] == 'medium':
    x_increment = 2
else:
    # This alien is really fast
    x_increment = 3

# Alien's new position
alien['x_position'] += x_increment

print(f"New Position: {alien['x_position']}")
print()


# Using the get() method to retrieve non-existent values
print(alien.get('points', 'No point value assigned'))

