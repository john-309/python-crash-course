# Exercise 8-12: Sandwiches
def build_sandwich(*toppings):
    """Print summary of all the toppings in a sandwich"""
    print("\nHere is your sandwich with the following toppings: ")
    for topping in toppings:
        print(f"- {topping.title()}")


build_sandwich('pepperoni', 'meatballs', 'ham')
build_sandwich('turkey', 'salami', 'chicken')
build_sandwich('pastrami')

print()


# Exercise 8-13: User Profile
def build_profile(first, last, **user_info):
        """Build a dictionary containing everything we know about a user."""
        user_info['first_name'] = first
        user_info['last_name'] = last
        return user_info

my_profile = build_profile('john', 'montes', location='california', 
    job='developer', favorite_color='purple')

print(my_profile)
print()


# Exercise 8-14: Cars
def car_profile(manufacturer, model, **car_info):
    """Build a dictionary containing information of a specific car."""
    car_info['make'] = manufacturer
    car_info['model'] = model
    return car_info

car_one = car_profile('toyota', 'tacoma', color='grey', tow_package=True)
car_two = car_profile('subaru', 'outback', color='blue', tow_package=False)

print(car_one)
print(car_two)
