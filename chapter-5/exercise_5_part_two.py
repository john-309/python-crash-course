## Exercise 5-3
alien_color = 'yellow'
#
#if alien_color == 'green':
#    points = 5
## Exercise 5-4, convert to if-else block
#else:
#    points = 10
#
#print(f"The alien's color is {alien_color}, you earned {points} points!")
#
#
## Exercise 5-5, same problem with if-elif-else
if alien_color == 'green':
    points = 5
elif alien_color == 'yellow':
    points = 10
else:
    points = 15

print(f"The alien's color is {alien_color}, you earned {points} points!")
print()


# Exercise 5-6
age = 29

if age < 2:
    print("You are a baby.")
elif age < 4:
    print("You are a toddler.")
elif age < 13:
    print("You are a kid.")
elif age < 20:
    print("You are a teenager.")
elif age < 65:
    print("You are an adult.")
elif age >= 65:
    print("You are an elder.")

print()


# Exercise 5-7
fav_fruits = ['apple', 'oranges', 'cherry', 'mango', 'grapefruit']

if 'apple' in fav_fruits:
    print("You really like apples!")
if 'banana' in fav_fruits:
    print("You really like bananas!")
if 'watermelon' not in fav_fruits:
    print("You don't like watermelon?")

