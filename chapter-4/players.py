# Print a slice of a list
players = ['martin', 'sean', 'kevin', 'alex', 'john', 'daniel']
print("The first three players in the list")
print(players[0:3])
print()

print("\nPrint players two through four")
print(players[1:4])


print("\nThe last three players in the list")
print(players[-3:])         # Omit the second index to print until the end of list


# Looping though a subset list
print("\nHere is the roster of the first three players")
for player in players[0:3]:
    print(player.title())
