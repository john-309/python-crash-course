# Exercise 8-1 Message
def display_message():
    print("I am learning how to create functions!")

display_message()


# Exercise 8-2 Favorite Book
def favorite_book(title):
    print(f"One of my favorite books is {title.title()}.")

favorite_book('hackers')
