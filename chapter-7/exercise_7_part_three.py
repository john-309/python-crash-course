# Exercise 7-8: Deli
sandwich_orders = ['turkey', 'pastrami', 'salami', 'all-meat', 'pastrami', 'blt', 'low-carb', 'chicken', 'pastrami']
finished_sandwiches = []

print("We have run out of pastrami sandwiches.\n")
while 'pastrami' in sandwich_orders:
    sandwich_orders.remove('pastrami')


while sandwich_orders:
    current_sandwich = sandwich_orders.pop()
    
    print(f"Making your {current_sandwich.title()} sandwich!")
    finished_sandwiches.append(current_sandwich)

print("\nThe following sandwiches have been made: ")

for sandwich in finished_sandwiches:
    print(sandwich.title())

print()

# Exercise 7-9: No Pastrami
# Extend exercise 7-8 by adding multiple pastrami sandwiches in list and then
# using a while loop to remove pastrami sandwiches


# Exercise 7-10: Dream Vacation
dream_vacation = {}

# Flag to keep loop active
active = True

while active:
    name = input("Type in your name: ")
    vacation = input(f"Hello {name.title()}, where would you like to go for vacation? ")

    dream_vacation[name] = vacation

    repeat = input("Anyone else want to answer the poll? (yes/no) ")
    if repeat == 'no':
        active = False

print("\nThese are the Poll results: ")
for name, vacation in dream_vacation.items():
    print(f"{name.title()} would like to go to {vacation.title()}.")

