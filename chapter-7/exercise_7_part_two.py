# 7-4 Pizza Toppings
prompt = "\nEnter a topping you'd like on your pizza:"
prompt += "\n(Enter 'quit' when done.) "

message = ""

while message != 'quit':
    message = input(prompt)

    if message != 'quit':
        print(f"Adding {message.title()} to your pizza.")


print("Your pizza is done!")


# 7-5 Movie Tickets:
prompt = "\nTicket prices depend on age. How old are you?"
prompt += "\n(Enter 'quit' when done.) "


while True:
    message = input(prompt)
    
    if message == 'quit':
        break

    message = int(message)
    if message < 3:
        print("Your ticket is free!")
    elif 3 <= message <= 12:
        print("Your ticket is $10.")
    elif message > 12:
        print("Your ticket is $15.")

