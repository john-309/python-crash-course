# Populate and empty list with the squares of a range
squares = []

for value in range(1, 10):
    squares.append(value**2)

print("These are the squared values of a list: ")
print(squares)


# Create the same output using a list comprehension
squares = [value**2 for value in range(1, 10)]
print("\nSame output using a list comprehension")
print(squares)

