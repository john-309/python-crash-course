favorite_languages = {
    'jen': 'python',
    'sarah': 'c',
    'edward': 'ruby',
    'phil': 'python',
    }

#for name, language in favorite_languages.items():
#    print(f"{name.title()}'s favorite programming language is {language.title()}")
#
#print()
#
# Using the keys() method to print the list of names in dictionary
friends = ['sarah', 'phil']

for name in favorite_languages.keys():
    print(f"Hi {name.title()}!")

    if name in friends:
        language = favorite_languages[name]
        print(f"\t{name.title()}, I see you like {language.title()}.")


# Print only the values
print("\nThe following are the chosen languages in the dictionary:")
for language in set(favorite_languages.values()):
    print(language.title())
