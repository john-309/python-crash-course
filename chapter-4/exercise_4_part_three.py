# Exercise 4-10
random_list = ['cars', 'motorcycles', 'food', 'bed', 'house', 'soda', 'water']

print("The first three items in the list are:")
print(random_list[:3])

print("\nThree items from the middle of the list are:")
print(random_list[2:5])

print("\nThe last three items in the list are:")
print(random_list[-3:])
print()


# Exercise 4-11
my_pizzas = ['pepperoni', 'hawaiian', 'chicken']
friend_pizzas = my_pizzas[:]

my_pizzas.append('sausage')
friend_pizzas.insert(1, 'cheese')

print("\nMy favorite pizzas are:")
for pizza in my_pizzas:
    print(pizza)

print("\nMy friend's favorite pizzas are:")
for pizza in friend_pizzas:
    print(pizza)

