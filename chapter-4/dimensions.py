dimensions = (200, 50)
print("Original Dimensios:")
print(dimensions[0])
print(dimensions[1])

# Altering a tuple is not allowed
# dimensions[0] = 100

# Writing over tuple (reassigning variables for entire tuple)
dimensions = (250, 100)

print("\nThese are the new dimensions for the tuple:")
for dimension in dimensions:
    print(dimension)
