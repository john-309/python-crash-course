# Exercise 4-3: Counting to Twenty
numbers = []

for value in range(1, 21):
    numbers.append(value)

print(numbers)


# Exercise 4-4: One Million
numbers = [list(range(1, 1000001))]

for number in numbers:
    print(number)


# Exercise 4-5: Summing a Million
numbers = list(range(1, 1000001))
print(min(numbers))
print(max(numbers))
print(sum(numbers))


# Exercise 4-6: Odd Numbers
odd_numbers = list(range(1, 21, 2))

for number in odd_numbers:
    print(number)


# Exercise 4-7: Threes
multiples_three = list(range(3, 31, 3))

for number in multiples_three:
    print(number)


# Exercise 4-8: Cubes
cubed_numbers = []

for value in range(1, 11):
    cubed_numbers.append(value**3)

print(cubed_numbers)


# Exercise 4-9: Cube Comprehension
cubed_numbers = [number**3 for number in range(1, 11)]
print("\nUsing a list comprehension: ")
print(cubed_numbers)

