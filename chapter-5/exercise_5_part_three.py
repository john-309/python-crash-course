## Exercise 5-8
usernames = ['jade', 'carol', 'admin', 'lyla', "eric"]
#
#for user in usernames:
#    if user == 'admin':
#        print(f"Hello {user.lower()}, would you like to see a status report?")
#    else:
#        print(f"Hello {user.title()}, welcome back!")
#
#
# Exercise 5-9
#usernames = []
#
#if usernames:
#    for user in usernames:
#        if user == 'admin':
#            print(f"Hello {user.lower()}, would you like to see a status report?")
#        else:
#            print(f"Hello {user.title()}, welcome back!")
#else:
#    print("We need to find users!")


# Exercise 5-10
new_usernames = ['adela', 'michael', 'jAde', 'eric', 'sam']

for user in new_usernames:
    user = user.lower()
    if user in usernames:
        print(f"Username {user.title()} already exists, try a different one.")
    else:
        print(f"Username {user.title()} is available!")
print()


# Exercise 5-11
numbers = list(range(1, 10))

for number in numbers:
    if number == 1:
        print(f"{number}st")
    elif number == 2:
        print(f"{number}nd")
    elif number == 3:
        print(f"{number}rd")
    else:
        print(f"{number}th")
