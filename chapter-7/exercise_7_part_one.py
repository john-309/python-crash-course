# Exercise 7-1 Rental Car
user_car = input("What car would you like to rent today? ")
print(f"\nLet me see if we have a {user_car.title()}.")


# Exercise 7-2 Restaurant Seating
dinner_group = input("Hello, how many people are in your group? ")
dinner_group = int(dinner_group)

if dinner_group > 8:
    print("\nYou'll have to wait a few minutes for a table.")
else:
    print("\nYour table is ready!")


# Exercise 7-3 Multiples of Ten
number = input("Enter a number and I will tell you if it's a multiple of 10: ")
number = int(number)

if number % 10 == 0:
    print(f"\nThe number {number} is a multiple of 10.")
else:
    print(f"\nThe number {number} is not a multiple of 10.")
