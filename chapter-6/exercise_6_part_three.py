# Exercise 6-7
person_1 = {
        'first_name': 'alex',
        'last_name' : 'garcia',
        'age' : 10,
        'city' : 'stanton',
        }

person_2 = {
        'first_name' : 'china',
        'last_name' : 'montes',
        'age' : 21,
        'city' : 'riverside',
        }


person_3 = {
        'first_name' : 'diana',
        'last_name' : 'ochoa',
        'age' : 30,
        'city' : 'orange',
        }


people = [person_1, person_2, person_3]

for person in people:
    print(f"\nThis is {person['first_name']}'s info:")
    for key, value in person.items():
        print(key,":", value)


# Exercise 6-9
favorite_places = {
        'dave' : ['chicago', 'new york', 'hawaii'],
        'chris' : ['las vegas', 'channel islands', 'cancun'],
        'danielle' : ['paris', 'seal beach', 'vancouver'],
        }

for friend, places in favorite_places.items():
    print(f"\nThese are {friend.title()}'s favorite places:")
    print(places)


# Exercise 6-11
cities = {
        'orange' : {
            'country' : 'usa',
            'fact' : 'no fact',
            'population' : 'who knows',
            },
        'santa ana' : {
            'country' : 'usa',
            'fact' : 'very diverse',
            'population' : 'over-populated',
            },
        }

for city, city_info in cities.items():
    print(f"\nCity : {city.title()}")

    for key, value in city_info.items():
        print(f"{key.title()} : {value.title()}")


