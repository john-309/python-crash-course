# 6-1 Person
person = { 
    'first_name' : 'Alex',
    'last_name' : 'Garcia',
    'age' : 24,
    'city' : 'alhambra'
    }

print(person)
print()

# 6-2 Favorite Numbers
favorite_numbers = {
    'chano' : 10,
    'titi' : 8,
    'adalyne': 21,
    'chubbies' : 15,
    'irving' : 71,
}

print(favorite_numbers)
print(f"\nIrving's favorite number is: {favorite_numbers['irving']}")
