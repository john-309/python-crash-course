# Exercise 9-3: Create a User class
class User:

    """ A simple User class that stores a user's full name. """
    def __init__(self, first_name, last_name, age, music):
        self.first_name = first_name
        self.last_name = last_name
        self.age = age
        self.music = music


    def describe_user(self):
        print(f"\nThis is {self.first_name}'s information:")
        print(f"\tFirst Name: {self.first_name}")
        print(f"\tLast Name: {self.last_name}")
        print(f"\tAge: {self.age}")
        print(f"\tFavorite Music: {self.music}")
        
    def greet_user(self):
        print(f"\nWelcome {self.first_name} to the club!")


person_1 = User('Alice', 'Stefan', 21, 'Rock')
person_2 = User('Dave', 'Garcia', 18, 'Reggae')
person_3 = User('Nia', 'Cordoba', 28, 'Pop')

person_1.describe_user()
person_1.greet_user()

person_2.describe_user()
person_2.greet_user()

person_3.describe_user()
person_3.greet_user()
