# Exercise 4-1
toppings = ['pepperoni', 'hawaiian', 'chicken']

for pizza in toppings:
    print(f"I reallly like {pizza.title()} pizza!")

print("\nI really love pizza!")
print()


# Exercise 4-2

pets = ['cat', 'dog', 'bunny']

for pet in pets:
    print(f"A {pet.title()} would make a great pet!")

print("\nAny of these would make a great pet!")

