# Exercise 8-3: T-Shirt
def make_shirt(size, message):
    print(f"You want a size {size.title()} t-shirt that says: {message.title()}.")

make_shirt('medium', 'i love pizza')
make_shirt(message='do work', size='large')


# Exercise 8-4: Large Shirts
def make_shirt_two(size='large', message='i love python'):
    print(f"You want a size {size.title()} t-shirt that says: {message.title()}.")

make_shirt_two(size='small')


# Exercise 8-5: Cities
def describe_city(city, country='united states'):
    print(f"{city.title()} is in {country.title()}.")

describe_city('los angeles')
describe_city(country='mexico', city='acapulco')
describe_city(city='amsterdam')
