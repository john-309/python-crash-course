# Simulate printing each design, until none are left.
# Move each design to completed_models after printing.
def print_design(unprinted_designs, completed_models):
    """This function runs through list and appends to new list"""
    while unprinted_designs:
        current_design = unprinted_designs.pop()
        print(f"Printing model: {current_design}")
        completed_models.append(current_design)

# Display all completed models.
def complete_design(completed_models):
    """Print out the new completed list"""
    print("\nThe following models have been printed:")
    for completed_model in completed_models:
        print(completed_model)
